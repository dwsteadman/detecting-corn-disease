from PIL import Image
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.feature import hog

img = np.asarray(Image.open("MLN/MLN_10.JPEG"))
# We don't have to use RGB channels to extract features, Grayscale is enough.
img = cv2.cvtColor(cv2.resize(img,(160,160)),cv2.COLOR_RGB2GRAY)
fd,img = hog(img, orientations=9, pixels_per_cell=(8, 8),
                    cells_per_block=(4, 4), visualize=True)

plt.imshow(img, cmap=plt.cm.gray)


plt.show()
