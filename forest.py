from glob import glob
import numpy as np
import time

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from skimage.feature import hog

mln_paths = glob("MLN/*")
healthy_paths = glob("HEALTHY/*")

pos_labels = np.ones(len(mln_paths))
neg_labels = np.zeros(len(healthy_paths))

with open("hog.npy", "rb") as hog:
    x = np.load(hog)
    y = np.asarray(list(pos_labels) + list(neg_labels))

x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.2,random_state=42)


param_grid = {
    'bootstrap': [True],
    'max_depth': [80, 90, 100, 110],
    'max_features': [2, 3],
    'min_samples_leaf': [3, 4, 5],
    'min_samples_split': [8, 10, 12],
    'n_estimators': [100, 200, 300, 1000]
}

#grid = GridSearchCV(estimator=RandomForestClassifier(), param_grid=param_grid, verbose=3, cv=5, n_jobs=-1)
grid = RandomForestClassifier()
grid.fit(x_train, y_train)
#print(grid.best_estimator_)

start = time.time()
y_pred = grid.predict(x_test)
end = time.time()

print(classification_report(y_test, y_pred))
print("Predictions per second:", len(y_pred) / (end-start))
