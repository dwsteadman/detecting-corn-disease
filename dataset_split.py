import os
import shutil

cwd = os.getcwd()

TRAINING_MAX = 7670 * .8

for i, filename in enumerate(os.listdir("DRONE_IMAGES")):
    target = "/images/train/" if i <= TRAINING_MAX else "/images/validate/"
    src = cwd + "/DRONE_IMAGES/" + filename
    dst = cwd + target + filename

    shutil.move(src, dst)
