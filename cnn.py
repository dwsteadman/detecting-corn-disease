import pandas as pd
import numpy as np
import time

import cv2
from glob import glob
import matplotlib.pyplot as plt
import pickle
from PIL import Image
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras import datasets, layers, models, optimizers
from tqdm import tqdm


with open("norm_imgs.pkl", "rb") as pkl:
    packed = pickle.load(pkl)
    x = packed.get("imgs")
    y = packed.get("labels")


# Build CNN
x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.2,random_state=42)

#print(x_train.shape)

model = models.Sequential()
model.add(tf.keras.Input(shape=(160,160,3,)))
model.add(layers.Conv2D(16, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(24, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(32, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(48, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))

model.add(layers.Flatten())
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(1, activation="sigmoid"))

optimizer = optimizers.RMSprop(learning_rate=0.001)
model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=['accuracy'])

history = model.fit(x_train, y_train, epochs=10, batch_size=24, validation_data=(x_test, y_test))

plt.plot(history.history['accuracy'], label='accuracy')
plt.plot(history.history['val_accuracy'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')
plt.show()

start = time.time()
test_loss, test_acc = model.evaluate(x_test,  y_test, verbose=2)
end = time.time()

print("Predictions per second:", len(x_test) / (end-start))
