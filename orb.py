import cv2

start = time.time()

for mln_path in tqdm(mln_paths):
    img = np.asarray(Image.open(mln_path))
    # We don't have to use RGB channels to extract features, Grayscale is enough.
    img = cv2.cvtColor(cv2.resize(img,(160,160)),cv2.COLOR_RGB2GRAY)
    img = hog(img,orientations=9,pixels_per_cell=(20,20),
            cells_per_block=(2,2)
            )

    pos_images.append(img)

for healthy_path in tqdm(healthy_paths):
    img = np.asarray(Image.open(healthy_path))
    img = cv2.cvtColor(cv2.resize(img,(160, 160)),cv2.COLOR_RGB2GRAY)
    img = hog(img,orientations=9,pixels_per_cell=(20,20),
            cells_per_block=(2,2)
            )

    neg_images.append(img)

x = np.asarray(pos_images + neg_images)
y = np.asarray(list(pos_labels) + list(neg_labels))

end = time.time()
