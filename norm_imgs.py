import pandas as pd
import numpy as np

import cv2
from glob import glob
import pickle
from PIL import Image
from tqdm import tqdm


mln_paths = glob("MLN/*")
healthy_paths = glob("HEALTHY/*")

pos_images = []
neg_images = []

pos_labels = np.ones(len(mln_paths))
neg_labels = np.zeros(len(healthy_paths))

for mln_path in tqdm(mln_paths):
    img = np.asarray(Image.open(mln_path))
    # We don't have to use RGB channels to extract features, Grayscale is enough.
    img = cv2.resize(img,(160,160))
    img = img / 255.0

    pos_images.append(img)

for healthy_path in tqdm(healthy_paths):
    img = np.asarray(Image.open(healthy_path))
    img = cv2.resize(img,(160, 160))
    img = img / 255.0

    neg_images.append(img)

x = np.asarray(pos_images + neg_images)
y = np.asarray(list(pos_labels) + list(neg_labels))

imgs = {"imgs": x, "labels": y}

with open("norm_imgs.pkl", "wb") as pkl:
    pickle.dump(imgs, pkl)
