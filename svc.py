import pandas as pd
import numpy as np
import time

from glob import glob
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from skimage.feature import hog

from PIL import Image
import cv2
import pickle
from tqdm import tqdm

SAVE_HOG = False
SAVE_MODEL = False

USE_SAVED_HOG = True

mln_paths = glob("MLN/*")
healthy_paths = glob("HEALTHY/*")

pos_images = []
neg_images = []

pos_labels = np.ones(len(mln_paths))
neg_labels = np.zeros(len(healthy_paths))

if USE_SAVED_HOG:
    with open("hog.npy", "rb") as hog:
        x = np.load(hog)
        y = np.asarray(list(pos_labels) + list(neg_labels))

else:

    start = time.time()

    for mln_path in tqdm(mln_paths):
        img = np.asarray(Image.open(mln_path))
        # We don't have to use RGB channels to extract features, Grayscale is enough.
        img = cv2.cvtColor(cv2.resize(img,(160,160)),cv2.COLOR_RGB2GRAY)
        img /= 255

        pos_images.append(img)

    for healthy_path in tqdm(healthy_paths):
        img = np.asarray(Image.open(healthy_path))
        img = cv2.cvtColor(cv2.resize(img,(160, 160)),cv2.COLOR_RGB2GRAY)
        img /= 255

        neg_images.append(img)

    x = np.asarray(pos_images + neg_images)
    y = np.asarray(list(pos_labels) + list(neg_labels))

    end = time.time()
    print("HOG feature extraction time:", end-start)
    print("HOG extractions per second:", len(x)/(end-start))


# Save HOG
if SAVE_HOG:
    with open("hog.npy", "wb") as hog:
        np.save(hog, x)
        print("HOG features saved to 'hog.npy'")



# Run training
x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.2,random_state=42)

svc = SVC(C=10, gamma=0.5)

"""
# defining parameter range
param_grid = {'C': [10],
              'gamma': [0.505, 0.51, 0.495, 0.49],
              'kernel': ['rbf']}

grid = GridSearchCV(SVC(), param_grid, refit=True, verbose = 3)

# fitting the model for grid search
grid.fit(x_train, y_train)
print(grid.best_params_)

#svc.fit(x_train,y_train)
"""

svc.fit(x_train, y_train)

start = time.time()
#y_pred = svc.predict(x_test)
y_pred = svc.predict(x_test)
end = time.time()

print(classification_report(y_test, y_pred))
print("Predictions per second:", len(y_pred) / (end-start))
